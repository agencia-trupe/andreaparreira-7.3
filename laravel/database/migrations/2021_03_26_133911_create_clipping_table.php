<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateClippingTable extends Migration
{
    public function up()
    {
        Schema::create('clipping', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->integer('tipo_id')->unsigned();
            $table->foreign('tipo_id')->references('id')->on('clipping_tipos')->onDelete('cascade');
            $table->string('slug');
            $table->string('titulo');
            $table->string('ano');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('clipping');
    }
}
