<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateClippingLinksTable extends Migration
{
    public function up()
    {
        Schema::create('clipping_links', function (Blueprint $table) {
            $table->id();
            $table->integer('clipping_id')->unsigned();
            $table->foreign('clipping_id')->references('id')->on('clipping')->onDelete('cascade');
            $table->string('capa');
            $table->string('link_ext');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('clipping_links');
    }
}
