<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->id();
            $table->string('frase');
            $table->text('texto');
            $table->string('video');
            $table->string('imagem_sobre_1');
            $table->string('imagem_sobre_2');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('home');
    }
}
