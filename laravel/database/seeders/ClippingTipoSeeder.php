<?php

use Illuminate\Database\Seeder;

class ClippingTipoSeeder extends Seeder
{
    public function run()
    {
        DB::table('clipping_tipos')->insert([
            'titulo' => 'Imagens',
            'texto' => '',
            'video' => '',
            'imagem_sobre_1' => '',
            'imagem_sobre_2' => '',
        ]);
    }
}
