<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Painel\AceiteDeCookiesController;
use App\Http\Controllers\Painel\BannersController;
use App\Http\Controllers\Painel\ClippingsController;
use App\Http\Controllers\Painel\ClippingsImagensController;
use App\Http\Controllers\Painel\ClippingsVideosController;
use App\Http\Controllers\Painel\ClippingsLinksController;
use App\Http\Controllers\Painel\ConfiguracoesController;
use App\Http\Controllers\Painel\ContatoController;
use App\Http\Controllers\Painel\ContatosRecebidosController;
use App\Http\Controllers\Painel\HomeController;
use App\Http\Controllers\Painel\PainelController;
use App\Http\Controllers\Painel\PartnersController;
use App\Http\Controllers\Painel\SobreController;
use App\Http\Controllers\Painel\ServicosController;
use App\Http\Controllers\Painel\PoliticaDePrivacidadeController;
use App\Http\Controllers\Painel\ProjetosCategoriasController;
use App\Http\Controllers\Painel\ProjetosController;
use App\Http\Controllers\Painel\ProjetosImagensController;
use App\Http\Controllers\Painel\ProjetosVideosController;
use App\Http\Controllers\Painel\UsuariosController;

use Illuminate\Support\Facades\Route;



Route::group([
    'prefix' => 'painel',
], function () {
    // AUTH - BREEZE
    Route::get('/login', [AuthenticatedSessionController::class, 'create'])
        ->middleware('guest')
        ->name('login');
    Route::post('/login', [AuthenticatedSessionController::class, 'store'])
        ->middleware('guest');
    Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
        ->middleware('guest')
        ->name('password.request');
    Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
        ->middleware('guest')
        ->name('password.email');
    Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
        ->middleware('guest')
        ->name('password.reset');
    Route::post('/reset-password', [NewPasswordController::class, 'store'])
        ->middleware('guest')
        ->name('password.update');
    Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
        ->middleware('auth')
        ->name('password.confirm');
    Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
        ->middleware('auth');
    Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->middleware('auth')
        ->name('logout');

    // PAINEL
    Route::group([
        'middleware' => ['auth']
    ], function () {
        Route::get('/', [PainelController::class, 'index'])->name('painel');
        Route::resource('home', HomeController::class)->only(['index', 'update']);
        Route::post('image-upload', [PainelController::class, 'imageUpload']);
        Route::post('order', [PainelController::class, 'order']);
        Route::resource('usuarios', UsuariosController::class);
        Route::resource('configuracoes', ConfiguracoesController::class)->only(['index', 'update']);
        Route::resource('politica-de-privacidade', PoliticaDePrivacidadeController::class)->only(['index', 'update']);
        Route::get('aceite-de-cookies', [AceiteDeCookiesController::class, 'index'])->name('aceite-de-cookies.index');
        Route::resource('contatos', ContatoController::class)->only(['index', 'update']);
        Route::get('contatos-recebidos/{id}/toggle', [ContatosRecebidosController::class, 'toggle'])->name('contatos-recebidos.toggle');
        Route::resource('contatos-recebidos', ContatosRecebidosController::class)->only(['index', 'show', 'destroy']);
        Route::resource('banners', BannersController::class);
        Route::resource('perfil', SobreController::class)->only(['index', 'update']);
        Route::resource('partners', PartnersController::class);
        Route::resource('servicos', ServicosController::class)->only(['index', 'update']);
        Route::resource('categorias', ProjetosCategoriasController::class)->parameters(['categorias', 'categorias_projetos']);
        Route::resource('projetos', ProjetosController::class);
        Route::resource('projetos.videos', ProjetosVideosController::class)->parameters(['videos', 'videos_projetos']);
        Route::get('projetos/{projeto}/imagens/clear', [ProjetosImagensController::class, 'clear'])->name('projetos.imagens.clear');
        Route::resource('projetos.imagens', ProjetosImagensController::class);
        
        Route::resource('clippings', ClippingsController::class);
        Route::get('clippings/{clipping}/imagens/clear', [ClippingsImagensController::class, 'clear'])->name('clippings.imagens.clear');
        Route::resource('clippings.imagens', ClippingsImagensController::class);
        Route::resource('clippings.links', ClippingsLinksController::class);
        Route::resource('clippings.videos', ClippingsVideosController::class);


        // Route::resource('grupos.servicos', ServicosController::class);
        // Route::get('trabalhe-conosco/{id}/toggle', [TrabalheConoscoController::class, 'toggle'])->name('trabalhe-conosco.toggle');
        // Route::resource('trabalhe-conosco', TrabalheConoscoController::class)->only(['index', 'show', 'destroy']);

        // Limpar caches
        Route::get('clear-cache', function () {
            $exitCode = Artisan::call('config:clear');
            $exitCode = Artisan::call('route:clear');
            $exitCode = Artisan::call('cache:clear');

            return 'DONE';
        });
    });
});