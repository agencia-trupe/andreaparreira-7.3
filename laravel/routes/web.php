<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SobreController;
use App\Http\Controllers\ServicosController;
use App\Http\Controllers\ProjetosController;
use App\Http\Controllers\ClippingController;
use App\Http\Controllers\PoliticaDePrivacidadeController;
use App\Http\Controllers\ContatoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('perfil', [SobreController::class, 'index'])->name('perfil'); 

Route::get('servicos', [ServicosController::class, 'index'])->name('servicos'); 

Route::get('projetos/{categoria_slug?}', [ProjetosController::class, 'index'])->name('projetos');
Route::get('projetos/{categoria_slug}/{projeto_slug}', [ProjetosController::class, 'show'])->name('projetos-show');

Route::get('clippings', [ClippingController::class, 'index'])->name('clippings');

Route::get('contato', [ContatoController::class, 'index'])->name('contato');
Route::post('contato', [ContatoController::class, 'post'])->name('contato.post');

Route::get('politica-de-privacidade', [PoliticaDePrivacidadeController::class, 'index'])->name('politica-de-privacidade');
Route::post('aceite-de-cookies', [HomeController::class, 'postCookies'])->name('aceite-de-cookies.post');



require __DIR__.'/auth.php';
