<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClippingRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'capa' => 'required|image',
            'descricao' => '',
            'link_ext' => '',
            'video' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
