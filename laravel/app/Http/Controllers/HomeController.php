<?php

namespace App\Http\Controllers;

use App\Models\Home;
use App\Models\Banner;
use App\Models\Imagem;
use App\Models\Destaque;
use App\Models\AceiteDeCookies;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.home', [
            'home' => Home::first(),
            'banners' => Banner::ordenados()->get(),
            'imagens' => Imagem::ordenados()->get(),
            'destaques' => Destaque::ordenados()->get(),
        ]);
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
