<?php

namespace App\Http\Controllers;

use App\Models\ProjetoCategoria;
use App\Models\Projeto;
use App\Models\Sustentabilidade;
use App\Models\ProjetoImagem;

class ProjetosController extends Controller
{
    public function __construct()
    {
        view()->share('categorias', ProjetoCategoria::ordenados()->get());
    }


    public function index($categoria_slug = null)
    {
        if ($categoria_slug == null) {
            $categoria = ProjetoCategoria::ordenados()->firstOrFail();
        } else {
            $categoria = ProjetoCategoria::where('slug', $categoria_slug)->first();
        }
        view()->share('categoria', $categoria);

        $projetos = $categoria->projetos;

        return view('frontend.projetos.index', compact('projetos'));
    }


    public function show($categoria_slug, $projeto_slug)
    {
        $projeto = Projeto::where('slug', $projeto_slug)->first();
        $imagens = ProjetoImagem::projeto($projeto->id)->ordenados()->get();
         
        return view('frontend.projetos.show', compact('projeto', 'imagens'));
    }

}
