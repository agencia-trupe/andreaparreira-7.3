<?php

namespace App\Http\Controllers;

use App\Models\Clipping;
use App\Models\ClippingGaleria;
use App\Models\ClippingImagem;
use App\Models\ClippingLink;
use App\Models\ClippingVideo;

class ClippingController extends Controller
{
    public function index()
    {
        // $clippings = Clipping::ordenados()
        //     ->join('clipping_tipos', 'clipping_tipos.id', '=', 'clipping.tipo_id')
        //     ->select('clipping_tipos.id as tipo_id', 'clipping_tipos.titulo as tipo_titulo', 'clipping.id as id', 'clipping.ordem as ordem', 'clipping.slug as slug', 'clipping.titulo as titulo', 'clipping.ano as ano')
        //     ->get();

        $clippingGaleria = Clipping::where('tipo_id', 1)->ordenados()->get();
        $clippingLinks = Clipping::where('tipo_id', 2)->ordenados()->get();
        $clippingVideos = Clipping::where('tipo_id', 3)->ordenados()->get();
        // dd($clippingLinks);

        $imagens = ClippingImagem::get();
        $links = ClippingLink::get();
        $videos = ClippingVideo::get();
        $linkVideo = "https://www.youtube.com/embed/";

        // dd($links);

        return view('frontend.clippings', compact('clippingGaleria', 'clippingVideos', 'clippingLinks', 'imagens', 'links', 'videos', 'linkVideo'));
    }
}
