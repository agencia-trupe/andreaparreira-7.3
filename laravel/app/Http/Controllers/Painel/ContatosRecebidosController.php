<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ContatoRecebido;

class ContatosRecebidosController extends Controller
{
    public function index()
    {
        $contatos = ContatoRecebido::orderBy('created_at', 'DESC')->get();

        return view('painel.contatos.recebidos.index', compact('contatos'));
    }

    public function show(ContatoRecebido $contatos_recebido)
    {
        $contatos_recebido->update(['lido' => 1]);

        return view('painel.contatos.recebidos.show', compact('contatos_recebido'));
    }

    public function destroy(ContatoRecebido $contatos_recebido)
    {
        try {

            $contatos_recebido->delete();
            return redirect()->route('contatos-recebidos.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }

    public function toggle(ContatoRecebido $contatos_recebido, Request $request)
    {
        try {

            $contatos_recebido->update([
                'lido' => !$contatos_recebido->lido
            ]);

            return redirect()->route('contatos-recebidos.index')->with('success', 'Mensagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: '.$e->getMessage()]);

        }
    }
}
