<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClippingTipoRequest;
use App\Models\Clipping;
use App\Models\ClippingGaleria;
use App\Models\ClippingTipo;
use App\Models\ClippingImagem;

class clippingsController extends Controller
{
    public function index(Clipping $registro)
    {
        $tipos = ClippingTipo::orderBy('id', 'ASC')->get();

        $clippings = Clipping::ordenados()->get();

        $capasLinks = Clipping::join('clipping_links', 'clipping_links.clipping_id', '=', 'clipping.id')
            ->select('clipping_links.clipping_id as clipping_id', 'clipping.tipo_id as tipo_id', 'clipping_links.capa as capa')
            ->get();

        $capasVideos = Clipping::join('clipping_videos', 'clipping_videos.clipping_id', '=', 'clipping.id')
            ->select('clipping_videos.clipping_id as clipping_id', 'clipping.tipo_id as tipo_id', 'clipping_videos.link_video as link_video')
            ->get();

        $capasImagens = Clipping::join('clipping_galerias', 'clipping_galerias.clipping_id', '=', 'clipping.id')
            ->select('clipping_galerias.clipping_id as clipping_id', 'clipping.tipo_id as tipo_id', 'clipping_galerias.imagem as imagem')->orderBy('clipping_galerias.ordem', 'ASC')
            ->get();

        $linkVideo = "https://www.youtube.com/embed/";

        return view('painel.clippings.index', compact('clippings', 'tipos', 'capasImagens', 'capasLinks', 'capasVideos','linkVideo'));
    }

    public function create()
    {
        $tipos = ClippingTipo::orderBy('id', 'ASC')->pluck('titulo', 'id');

        return view('painel.clippings.create', compact('tipos'));
    }

    public function store(ClippingTipoRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            Clipping::create($input);

            return redirect()->route('clippings.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Clipping $clipping)
    {
        $tipos = ClippingTipo::orderBy('id', 'ASC')->pluck('titulo', 'id');

        return view('painel.clippings.edit', compact('clipping', 'tipos'));
    }

    public function update(ClippingTipoRequest $request, clipping $clipping)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            $clipping->update($input);

            return redirect()->route('clippings.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Clipping $clipping)
    {
        try {
            $clipping->delete();

            return redirect()->route('clippings.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
