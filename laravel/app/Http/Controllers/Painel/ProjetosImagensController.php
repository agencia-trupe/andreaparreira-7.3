<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProjetosImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Projeto;
use App\Models\ProjetoImagem;

use App\Helpers\CropImage;

class ProjetosImagensController extends Controller
{
    public function index(Projeto $projeto)
    {
        $imagens = ProjetoImagem::projeto($projeto->id)->ordenados()->get();

        return view('painel.projetos.imagens.index', compact('imagens', 'projeto'));
    }

    public function show(Projeto $projeto, ProjetoImagem $imagen)
    {
        return $imagen;
    }

    public function store(Projeto $projeto, ProjetosImagensRequest $request)
    {
        try {
            $input = $request->all();
            $input['imagem'] = ProjetoImagem::uploadImagem();
            $input['projeto_id'] = $projeto->id;

            $imagem = ProjetoImagem::create($input);

            $view = view('painel.projetos.imagens.imagem', compact('projeto', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {
            return 'Erro ao adicionar imagem: ' . $e->getMessage();
        }
    }

    public function destroy(Projeto $projeto, ProjetoImagem $imagen)
    {
        try {
            $imagen->delete();
            return redirect()->route('projetos.imagens.index', $projeto->id)
                ->with('success', 'Imagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: ' . $e->getMessage()]);
        }
    }

    public function clear(Projeto $projeto)
    {
        try {
            $projeto->imagens()->delete();
            return redirect()->route('projetos.imagens.index', $projeto->id)
                ->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
