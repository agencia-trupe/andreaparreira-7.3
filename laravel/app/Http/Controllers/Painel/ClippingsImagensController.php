<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClippingImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Clipping;
use App\Models\ClippingImagem;

use App\Helpers\CropImage;

class ClippingsImagensController extends Controller
{
    public function index(Clipping $clipping)
    {
        $imagens = ClippingImagem::Clipping($clipping->id)->ordenados()->get();

        return view('painel.clippings.imagens.index', compact('imagens', 'clipping'));
    }

    public function show(Clipping $clipping, ClippingImagem $imagem)
    {
        return $imagem;
    }

    public function store(Clipping $clipping, ClippingImagensRequest $request)
    {
        try {
            $input = $request->all();
            $input['imagem'] = ClippingImagem::uploadImagem();
            $input['clipping_id'] = $clipping->id;

            $imagem = ClippingImagem::create($input);

            $view = view('painel.clippings.imagens.imagem', compact('clipping', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Clipping $clipping, ClippingImagem $imagen)
    {
       
        try {
            $imagen->delete();
            return redirect()->route('clippings.imagens.index', $clipping->id)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Clipping $clipping)
    {
        try {

            $clipping->imagens()->delete();
            return redirect()->route('clippings.imagens.index', $clipping->id)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
