<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests\ProjetosVideosRequest;
use App\Http\Controllers\Controller;

use App\Models\Projeto;
use App\Models\ProjetoVideo;

class ProjetosVideosController extends Controller
{
    public function index(Projeto $projeto)
    {
        $registros = $projeto->videos;

        return view('painel.projetos.videos.index', compact('projeto', 'registros'));
    }

    public function create(Projeto $projeto)
    {
        return view('painel.projetos.videos.create', compact('projeto'));
    }

    public function store(ProjetosVideosRequest $request, Projeto $projeto)
    {
        try {

            $input = $request->all();

            $projeto->videos()->create($input);

            return redirect()->route('projetos.videos.index', $projeto->id)
                ->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Projeto $projeto, ProjetoVideo $video)
    {
        return view('painel.projetos.videos.edit', compact('projeto', 'video'));
    }

    public function update(ProjetosVideosRequest $request, Projeto $projeto, ProjetoVideo $video)
    {
        try {

            $input = $request->all();

            $video->update($input);

            return redirect()->route('projetos.videos.index', $projeto->id)
                ->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Projeto $projeto, ProjetoVideo $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('projetos.videos.index', $projeto->id)
                ->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
