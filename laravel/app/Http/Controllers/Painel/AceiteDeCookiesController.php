<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\AceiteDeCookies;
use Illuminate\Http\Request;

class AceiteDeCookiesController extends Controller
{
    public function index()
    {
        $cookies = AceiteDeCookies::orderBy('created_at', 'DESC')->get();

        return view('painel.cookies.index', compact('cookies'));
    }
}
