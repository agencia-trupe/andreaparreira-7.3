<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Imagem;

class ImagensController extends Controller
{
    private $LIMITE_REGISTROS = 4;

    public function index()
    {
        $registros = Imagem::ordenados()->get();
        $limiteExcedido = $registros->count() >= $this->LIMITE_REGISTROS;

        return view('painel.imagens.index', compact('registros', 'limiteExcedido'));
    }

    public function create()
    {
        return view('painel.imagens.create');
    }

    public function store(ImagensRequest $request)
    {
        try {

            if (Imagem::count() >= $this->LIMITE_REGISTROS) {
                throw new \Exception('Limite de registros excedido.');
            }

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Imagem::upload_imagem();

            Imagem::create($input);

            return redirect()->route('painel.imagens.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Imagem $registro)
    {
        return view('painel.imagens.edit', compact('registro'));
    }

    public function update(ImagensRequest $request, Imagem $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Imagem::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.imagens.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Imagem $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.imagens.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
