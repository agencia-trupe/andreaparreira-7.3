<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\MidiasGaleriasImagensRequest;
use App\Models\Midia;
use App\Models\MidiaGaleria;
use App\Models\MidiaGaleriaImagem;

class MidiasGaleriasImagensController extends Controller
{
    public function index(Midia $midia, MidiaGaleria $galeria)
    {
        $imagens = MidiaGaleriaImagem::galeria($galeria->id)->ordenados()->get();

        return view('painel.midias.galeria.imagens.index', compact('midia', 'galeria', 'imagens'));
    }

    public function show(Midia $midia, MidiaGaleria $galeria, MidiaGaleriaImagem $imagem)
    {
        return $imagem;
    }

    public function store(MidiasGaleriasImagensRequest $request, Midia $midia, MidiaGaleria $galeria)
    {
        try {
            $input = $request->all();
            $input['imagem'] = MidiaGaleriaImagem::uploadImagem();
            $input['galeria_id'] = $galeria->id;

            $imagem = MidiaGaleriaImagem::create($input);

            $view = view('painel.midias.galeria.imagens.imagem', compact('midia', 'galeria', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: ' . $e->getMessage();
        }
    }

    public function destroy(Midia $midia, MidiaGaleria $galeria, MidiaGaleriaImagem $imagem)
    {
        try {
            $imagem->delete();

            return redirect()->route('painel.midias.galeria.imagens.index', $midia->id, $galeria->id)
                ->with('success', 'Imagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: ' . $e->getMessage()]);
        }
    }

    public function clear(Midia $midia, MidiaGaleria $galeria)
    {
        try {
            $galeria->imagens()->delete();

            return redirect()->route('painel.midias.galeria.imagens.index', $midia->id, $galeria->id)
                ->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
