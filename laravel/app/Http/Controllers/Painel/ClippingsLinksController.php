<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClippingLinksRequest;
use App\Models\Clipping;
use App\Models\ClippingLink;

class clippingsLinksController extends Controller
{
    public function index(Clipping $clipping)
    {
        $link = ClippingLink::where('clipping_id', $clipping->id)->first();

        return view('painel.clippings.link.index', compact('clipping', 'link'));
    }

    public function create(Clipping $clipping)
    {
        return view('painel.clippings.link.create', compact('clipping'));
    }

    public function store(ClippingLinksRequest $request, Clipping $clipping)
    {
        try {
            $input = $request->all();
            $input['clipping_id'] = $clipping->id;

            if (isset($input['capa'])) $input['capa'] = ClippingLink::upload_capa();

            ClippingLink::create($input);

            return redirect()->route('clippings.links.index', $clipping->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Clipping $clipping, ClippingLink $link)
    {
        return view('painel.clippings.link.edit', compact('clipping', 'link'));
    }

    public function update(ClippingLinksRequest $request, Clipping $clipping, ClippingLink $link)
    {
        try {
            $input = $request->all();
            $input['clipping_id'] = $clipping->id;

            if (isset($input['capa'])) $input['capa'] = ClippingLink::upload_capa();

            $link->update($input);

            return redirect()->route('clippings.links.index', $clipping->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Clipping $clipping, ClippingLink $link)
    {
        try {
            $link->delete();

            return redirect()->route('clippings.links.index', $clipping->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
