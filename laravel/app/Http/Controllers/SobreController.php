<?php

namespace App\Http\Controllers;

use App\Models\Sobre;
use App\Models\Partners;
use App\Models\Imagem;

class SobreController extends Controller
{
    public function index()
    {
        return view('frontend.perfil', [
            'perfil' => Sobre::first(),
            'partners' => Partners::ordenados()->get(),
            'imagens' => Imagem::ordenados()->get(),
        ]);
    }
}