<?php

namespace App\Http\Controllers;

use App\Models\Servicos;

class ServicosController extends Controller
{
    public function index()
    {
        return view('frontend.servicos', [
            'servicos' => Servicos::first()
        ]);
    }
}
