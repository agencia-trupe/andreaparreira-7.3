<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClippingTipo extends Model
{
    protected $table = 'clipping_tipos';

    protected $guarded = ['id'];

    public function midias()
    {
        return $this->hasMany('App\Models\Clipping', 'tipo_id')->ordenados();
    }
}
