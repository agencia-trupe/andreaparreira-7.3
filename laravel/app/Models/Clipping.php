<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clipping extends Model
{
    protected $table = 'clipping';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeTipos($query, $id)
    {
        return $query->where('tipo_id', $id);
    }
    
    public function imagens()
    {
        return $this->hasMany('App\Models\ClippingImagem', 'clipping_id')->ordenados();
    } 
}
