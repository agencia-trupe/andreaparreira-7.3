<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class SustentabilidadeImagem extends Model
{
    protected $table = 'sustentabilidade_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeSustentabilidade($query, $id)
    {
        return $query->where('sustentabilidade_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => 180,
                'path'    => 'assets/img/sustentabilidade/imagens/thumbs/'
            ],
            [
                'width'   => 830,
                'height'  => null,
                'path'    => 'assets/img/sustentabilidade/imagens/'
            ]
        ]);
    }
}
