<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class ClippingLink extends Model
{
    protected $table = 'clipping_links';

    protected $guarded = ['id'];

    public function scopeMidia($query, $id)
    {
        return $query->where('clipping_id', $id);
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/clippings/link/'
        ]);
    }
}
