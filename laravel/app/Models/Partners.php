<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Partners extends Model
{
    protected $table = 'partners';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            //alterar width se não encaixar
            'width'  => 170,
            'height' => null,
            'path'   => 'assets/img/partners/'
        ]);
    }
}