import axios from 'axios';

import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

// $('.banners').cycle({
//     slides: '>.slide',
// });

$('.scroll-down').click(event => {
    event.preventDefault();

    $('html, body').animate(
        {
            scrollTop: $('.banners').offset().top + $('.banners').height(),
        },
        1000
    );
});


 // FANCYBOX MIDIAS GALERIAS
 $(".fancybox, .projeto-imagem").fancybox({
  padding: 0,
  prevEffect: "fade",
  nextEffect: "fade",
  closeBtn: false,
  openEffect: "elastic",
  openSpeed: "150",
  closeEffect: "elastic",
  closeSpeed: "150",
  keyboard: true,
  helpers: {
    title: {
      type: "outside",
      position: "top",
    },
    overlay: {
      css: {
        background: "rgba(132, 134, 136, .88)",
      },
    },
  },
  mobile: {
    clickOutside: "close",
  },
  fitToView: false,
  autoSize: false,
  beforeShow: function () {
    this.maxWidth = "100%";
    this.maxHeight = "100%";
  },
});


$(".clipping2").click(function handler(e) {
  e.preventDefault();

  const id = $(this).data("galeria-id");

  if (id) {
    $(`a[rel=galeria-${id}]`)[0].click();
  }
});



function positionCategorias() {
    const $wrapper = $('.categorias');
    const $active = $wrapper.find('.active');

    const translate =
        $active.width() / 2 + $active.offset().left - $wrapper.offset().left;

    $wrapper.css('transform', `translateX(-${translate}px)`);
}

if ($('.categorias').length) {
    positionCategorias();
    setInterval(positionCategorias, 200);
}


// BTN VER MAIS - MIDIAS
var itensMidias = $("section.clippings .clipping-item");
var spliceItensQuantidade = 9;

if (itensMidias.length <= spliceItensQuantidade) {
  $(".btn-mais-clippings").hide();
}

var setDivMidias = function () {
  var spliceItens = itensMidias.splice(0, spliceItensQuantidade);
  $(spliceItens).show();
  if (itensMidias.length <= 0) {
    $(".btn-mais-clippings").hide();
  }
};

$(".btn-mais-clippings").click(function () {
  setDivMidias();
});

$("section.clippings .clipping-item").hide();
setDivMidias();

//Layout Masonry
$("section.clippings .masonry").masonryGrid({
  columns: 3,
});

// PROJETOS/IMAGENS - BTN SCROLL TOP
$(document).scroll(function (e) {
  e.preventDefault();
  $(".link-topo").toggle($(document).scrollTop() !== 0);
});
$(".link-topo").click(function (e) {
  e.preventDefault();
  $("html, body").animate({ scrollTop: $("body").offset().top }, 500);
});




  // AVISO DE COOKIES
$(document).ready(function() {
  
  $(".aviso-cookies").hide();

  if (window.location.href == routeHome) {
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
      var url = window.location.origin + window.location.pathname + "aceite-de-cookies";

      $.ajax({
        type: "POST",
        url: url,
        success: function (data, textStatus, jqXHR) {
          $(".aviso-cookies").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        },
      });
    });
  }
});
