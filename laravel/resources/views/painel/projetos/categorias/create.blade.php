@extends('painel.layout.template')

@section('content')

    <legend>
        <h2><small>Projetos /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'categorias.store']) !!}

        @include('painel.projetos.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
