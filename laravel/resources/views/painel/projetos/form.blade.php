@include('painel.layout.flash')

<div class="form-group">
    {!! Form::label('projetos_categoria_id', 'Categoria') !!}
    {!! Form::select('projetos_categoria_id', $categorias, old('projetos_categoria_id'), ['class' => 'form-control']) !!}
</div>



<div class="form-group mt-3">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>


<div class="mb-3 col-12 col-md-12 mt-3">
    {!! Form::label('capa', 'Capa') !!}
    @if($submitText == 'Alterar')
    @if($projeto->capa)
    <img src="{{ url('assets/img/projetos/'.$projeto->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group mt-3">
    {!! Form::label('local', 'Local') !!}
    {!! Form::text('local', null, ['class' => 'form-control']) !!}
</div>


{!! Form::submit($submitText, ['class' => 'btn btn-success mt-3']) !!}

<a href="{{ route('projetos.index') }}" class="btn btn-default btn-light mt-3">Voltar</a>
