@extends('painel.layout.template')

@section('content')

    <legend>
        <h2><small>Projetos / {{ $projeto->titulo }} /</small> Adicionar Vídeo</h2>
    </legend>

    {!! Form::open(['route' => ['projetos.videos.store', $projeto->id]]) !!}

        @include('painel.projetos.videos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
