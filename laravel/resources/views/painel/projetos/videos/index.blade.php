@extends('painel.layout.template')

@section('content')

    @include('painel.layout.flash')

    <a href="{{ route('projetos.index') }}" title="Voltar para Projetos" class="btn btn-secondary mb-3 col-2">
    &larr; Voltar para Projetos</a>

    <legend>
        <h2>
            <small>Projetos / {{ $projeto->titulo }} /</small> Vídeos

            <a href="{{ route('projetos.videos.create', $projeto->id) }}" class="btn btn-success col-2 mt-3" style="position:relative;overflow:hidden"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Vídeo</a>
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="projetos_videos">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
            <td>
                <a href="#" class="btn btn-dark btn-sm btn-move">
                    <i class="bi bi-arrows-move"></i>
                </a>
            </td>
                <td>{{ $registro->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('projetos.videos.destroy', $projeto->id, $registro->id), 'method' => 'delete')) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('projetos.videos.edit', [$projeto->id, $registro->id]) }}" class="btn btn-primary btn-sm pull-left pe-3">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete pe-3"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
