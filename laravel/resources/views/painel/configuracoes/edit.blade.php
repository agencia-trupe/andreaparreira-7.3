@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">CONFIGURAÇÕES</h2>
</legend>

{!! Form::model($registro, [
'route' => ['configuracoes.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.configuracoes.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection