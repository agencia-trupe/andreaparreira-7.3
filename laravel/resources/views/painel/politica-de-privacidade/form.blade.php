@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('texto', 'Texto - Política de Privacidade') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('painel') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>