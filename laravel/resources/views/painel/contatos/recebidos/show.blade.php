@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">CONTATOS RECEBIDOS</h2>
</legend>



<div class="mb-3 col-12">
    <label>Nome</label>
    <div class="well">{{ $contatos_recebido->nome }}</div>
</div>

<div class="mb-3 col-12">
    <label>E-mail</label>
    <div class="well">
        <button class="btn btn-dark btn-sm clipboard me-2" data-clipboard-text="{{ $contatos_recebido->email }}">
            <i class="bi bi-clipboard"></i>
        </button>
        {{ $contatos_recebido->email }}
    </div>
</div>

@if($contatos_recebido->telefone)
<div class="mb-3 col-12">
    <label>Telefone</label>
    <div class="well">{{ $contatos_recebido->telefone }}</div>
</div>
@endif

<div class="mb-3 col-12">
    <label>Mensagem</label>
    <div class="well-msg">{!! $contatos_recebido->mensagem !!}</div>
</div>


<div class="d-flex align-items-center mt-4">
    <a href="{{ route('contatos-recebidos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>

@stop