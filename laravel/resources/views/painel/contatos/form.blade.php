@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::text('email', null, ['class' => 'form-control input-text']) !!}
</div>

    <div class="mb-3 col-12">
        {!! Form::label('telefone', 'Telefone') !!}
        {!! Form::text('telefone', null, ['class' => 'form-control input-text']) !!}
    </div>


<div class="mb-3 col-12">
    {!! Form::label('instagram', 'Instagram (link)') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control input-text']) !!}
</div>


    <div class="mb-3 col-12">
        {!! Form::label('endereco', 'Endereço') !!}
        {!! Form::textarea('endereco', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
 

<div class="mb-3 col-12">
    {!! Form::label('google_maps', 'Código GoogleMaps') !!}
    {!! Form::text('google_maps', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('whatsapp', 'Whatsapp (DDD + Número sem espaços)') !!}
    {!! Form::text('whatsapp', null, ['class' => 'form-control']) !!}
</div>


<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('contatos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>