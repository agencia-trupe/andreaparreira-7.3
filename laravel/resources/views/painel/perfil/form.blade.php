@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($registro->imagem_1)
    <img src="{{ url('assets/img/sobre/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>


<div class="mb-3 col-12">
    {!! Form::label('texto_1', 'Título do Texto') !!}
    {!! Form::textarea('texto_1', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto_2', 'Texto de Perfil') !!}
    {!! Form::textarea('texto_2', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto_3', 'Título do Segundo Texto') !!}
    {!! Form::textarea('texto_3', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto_4', 'Segundo Texto de Perfil') !!}
    {!! Form::textarea('texto_4', null, ['class' => 'form-control editor-padrao']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}