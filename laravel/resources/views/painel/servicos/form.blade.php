@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    @if($registro->imagem_1)
    <img src="{{ url('assets/img/servicos/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    <br>
    {!! Form::label('imagem_1', 'Imagem 1') !!}
    {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
</div>

<div class="form-group mb-3">
    {!! Form::label('texto_1', 'Título do Serviço 1') !!}
    {!! Form::textarea('texto_1', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="form-group mb-3">
    {!! Form::label('texto_2', 'Texto do Serviço 1') !!}
    {!! Form::textarea('texto_2', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<hr class="mb-4 mt-4">


<div class="form-group mb-3 col-12 col-md-12">
    @if($registro->imagem_2)
    <img src="{{ url('assets/img/servicos/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    <br>
    {!! Form::label('imagem_2', 'Imagem do Serviço 2') !!}
    {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
</div>

<div class="form-group mb-3">
    {!! Form::label('texto_3', 'Título do Serviço 2') !!}
    {!! Form::textarea('texto_3', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="form-group mb-3">
    {!! Form::label('texto_4', 'Texto do Serviço 2') !!}
    {!! Form::textarea('texto_4', null, ['class' => 'form-control editor-padrao']) !!}
</div>


<hr>

<div class="mb-3 col-12 col-md-12">
    @if($registro->imagem_3)
    <img src="{{ url('assets/img/servicos/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    <br>
    {!! Form::label('imagem_3', 'Imagem 3') !!}
    {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
</div>

<div class="form-group mb-3">
    {!! Form::label('texto_5', 'Título do Serviço 3') !!}
    {!! Form::textarea('texto_5', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="form-group mb-3">
    {!! Form::label('texto_6', 'Texto do Serviço 3') !!}
    {!! Form::textarea('texto_6', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
