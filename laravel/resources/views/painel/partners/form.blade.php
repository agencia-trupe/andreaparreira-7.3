@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/partners/'.$partner->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    <br>
    {!! Form::label('imagem', 'Imagem') !!}
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('links', 'Link do site do Parceiro') !!}
    {!! Form::text('links', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success  mt-3 mb-3 col-1']) !!}

<a href="{{ route('partners.index') }}" class="btn btn-default btn-primary col-1">Voltar</a>
