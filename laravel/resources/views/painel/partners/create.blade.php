@extends('painel.layout.template')

@section('content')

    <legend>
        <h2><small>Parceiros /</small> Adicionar Parceiro</h2>
    </legend>

    {!! Form::open(['route' => 'partners.store', 'files' => true]) !!}

        @include('painel.partners.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
