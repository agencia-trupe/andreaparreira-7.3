@extends('painel.layout.template')

@section('content')

    @include('painel.layout.flash')

    <legend>
        <h2>
            Parceiros
            <a href="{{ route('partners.create') }}" class="btn btn-success float-md-end"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Parceiro</a>
        </h2>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="partners">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Imagem</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog">Gerenciar</span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-dark btn-sm btn-move">
                        <i class="bi bi-arrows-move"></i>
                    </a>
                </td>
                <td><img src="{{ asset('assets/img/partners/'.$registro->imagem) }}" style="width: 100%; max-width:150px;" alt=""></td>
                <td class="crud-actions" style="width: 160px;">
                    {!! Form::open([
                        'route'  => ['partners.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('partners.edit', $registro->id ) }}" class="btn btn-primary btn-sm float-md-end pe-3">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete pe-3"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
