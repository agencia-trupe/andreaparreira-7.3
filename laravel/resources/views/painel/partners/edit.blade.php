@extends('painel.layout.template')

@section('content')

    <legend>
        <h2><small>Parceiros /</small> Editar Parceiro</h2>
    </legend>

    {!! Form::model($partner, [
        'route'  => ['partners.update', $partner->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.partners.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
