@extends('painel.layout.template')

@section('content')

<legend>
    <h2><small>Clippings |</small> Adicionar Clipping</h2>
</legend>

{!! Form::open(['route' => 'clippings.store', 'files' => true]) !!}

@include('painel.clippings.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection