@extends('painel.layout.template')

@section('content')

    @include('painel.layout.flash')

    <a href="{{ route('clippings.index') }}" title="Voltar para Clippings" class="btn btn-primary btn-sm col-2 mb-4">
        &larr; Voltar para Clippings    </a>

    <legend>
        <h2>
            <small>Clippings / Imagens:</small> {{ $clipping->titulo }}

            {!! Form::open(['route' => ['clippings.imagens.store', $clipping->id], 'files' => true, 'class' => 'pull-right']) !!}
            <div class="btn-group btn-group-sm pull-right">
                <span class="btn btn-success btn-sm mt-4" style="position:relative;overflow:hidden">
                    <span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>
                    Adicionar Imagens
                    <input id="images-upload" type="file" name="imagem" id="imagem" multiple style="position:absolute;top:0;right:0;opacity:0;font-size:200px;cursor:pointer;">
                </span>

                <a href="{{ route('clippings.imagens.clear', $clipping->id) }}" class="btn btn-danger btn-sm btn-delete btn-delete-link btn-delete-multiple mt-4">
                    <span class="glyphicon glyphicon-trash" style="margin-right:10px;"></span>
                    Limpar
                </a>
            </div>
            {!! Form::close() !!}
        </h2>
    </legend>

    <div class="progress progress-striped active">
        <div class="progress-bar" style="width: 0"></div>
    </div>

    <div class="alert alert-block alert-danger errors" style="display:none"></div>

    <div class="alert alert-info" style="display:inline-block;padding:10px 15px;">
        <small>
            <span class="glyphicon glyphicon-move" style="margin-right: 10px;"></span>
            Clique e arraste as imagens para ordená-las.
        </small>
    </div>

    <div id="imagens" data-table="clipping_galerias" style="display: flex;">
    @if(!count($imagens))
        <div class="alert alert-warning no-images" role="alert">Nenhuma imagem cadastrada.</div>
    @else
        @foreach($imagens as $imagem)
        @include('painel.clippings.imagens.imagem')
        @endforeach
    @endif
    </div>

@endsection
