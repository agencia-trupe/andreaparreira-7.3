@extends('painel.layout.template')

@section('content')

<legend>
    <h2><small>Clipping | Vídeo (YouTube) |</small> Adicionar Vídeo</h2>
</legend>

{!! Form::model($clipping, [
'route' => ['clippings.videos.store', $clipping->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.clippings.video.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection