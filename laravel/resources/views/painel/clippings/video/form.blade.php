@include('painel.layout.flash')

<div class="form-group">
    {!! Form::label('link_video', 'Vídeo do YouTube') !!}
    {!! Form::text('link_video', null, ['class' => 'form-control']) !!}
    <p class="observacao" style="color: red; font-style: italic;">Incluir aqui somente a parte da url após o "v=". Exemplo: <span style="text-decoration: underline;">https://www.youtube.com/watch?v=<strong>Y4goaZhNt4k</strong></span></p>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('clippings.videos.index', $clipping->id) }}" class="btn btn-sm btn-secondary mt-3" style="width: 71px;">Voltar</a>