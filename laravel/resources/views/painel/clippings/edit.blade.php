@extends('painel.layout.template')

@section('content')

<legend>
    <h2><small>Clippings |</small> Editar Mídia</h2>
</legend>

{!! Form::model($clipping, [
'route' => ['clippings.update', $clipping->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.clippings.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection