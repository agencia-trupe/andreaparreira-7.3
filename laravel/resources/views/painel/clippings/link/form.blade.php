@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/clippings/link/'.$link->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    <br>
    {!! Form::label('capa', 'Capa') !!}
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link_ext', 'Link para conteúdo externo') !!}
    {!! Form::text('link_ext', null, ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('clippings.links.index', $clipping->id) }}" class="btn btn-light mt-3">Voltar</a>