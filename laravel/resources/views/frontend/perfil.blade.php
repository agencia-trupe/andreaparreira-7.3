@extends('frontend.common.template')

@section('content')

    <div class="sobre">
        <div class="perfil-hr"></div>
        <div class="center-full">
            <div class="section-1">
                <div class="home-line2"></div>
                <div class="imagem" style="background-image:url({{ asset('assets/img/sobre/'.$perfil->imagem_1) }})"></div>
                <div class="perfil-bloco-1">
                    <div class="perfil-titulo">{!! $perfil->texto_1 !!}</div>
                    <div class="perfil-texto">{!! $perfil->texto_2 !!}</div>
                    <br>
                    <div class="perfil-titulo">{!! $perfil->texto_3 !!}</div>
                    <div class="perfil-texto">{!! $perfil->texto_4 !!}</div>
                </div>
            </div>

            <hr>
            
            <div class="section-2">
                <p>PARCEIROS</p>
                <div class="perfil-parceiros">      
                    @foreach($partners as $p)
                    @if($p->links)
                        <a href="{{ Tools::parseLink($p->links) }}" class="parceiros" style="background-image:url({{ asset('assets/img/partners/'.$p->imagem) }}">
                        </a>
                    @else
                        <div class="parceiros" style="background-image:url({{ asset('assets/img/partners/'.$p->imagem) }}"></div>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>


@endsection
