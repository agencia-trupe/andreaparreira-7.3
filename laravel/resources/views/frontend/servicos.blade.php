@extends('frontend.common.template')

@section('content')

    <div class="servicos">
        <div class="center-full">
            <div class="section-1">
            <div class="servicos-line"></div>
                <div class="servicos-card">
                    <div class="imagem" style="background-image:url({{ asset('assets/img/servicos/'.$servicos->imagem_1) }})"></div>
                    <div class="servicos-bloco">
                        <div class="servicos-titulo">{!! $servicos->texto_1 !!}</div>
                        <div class="servicos-texto">{!! $servicos->texto_2 !!}</div>
                    </div>
                </div>

                <div class="servicos-card">
                    <div class="imagem" style="background-image:url({{ asset('assets/img/servicos/'.$servicos->imagem_2) }})"></div>
                    <div class="servicos-bloco">
                        <div class="servicos-titulo">{!! $servicos->texto_3 !!}</div>
                        <div class="servicos-texto">{!! $servicos->texto_4 !!}</div>
                    </div>
                </div>

                <div class="servicos-card">
                    <div class="imagem" style="background-image:url({{ asset('assets/img/servicos/'.$servicos->imagem_3) }})"></div>
                    <div class="servicos-bloco">
                        <div class="servicos-titulo">{!! $servicos->texto_5 !!}</div>
                        <div class="servicos-texto">{!! $servicos->texto_6 !!}</div>
                    </div>
                </div>

            </div>

           
        </div>
    </div>

@endsection
