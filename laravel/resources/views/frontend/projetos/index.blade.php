@extends('frontend.common.template')

@section('content')

    <div class="projetos">
        <div class="center-full">
            

            <div class="index-grid">
                @foreach($projetos as $p)
                <a href="{{ route('projetos-show', ['categoria_slug' => $p->categoria->slug, 'projeto_slug' => $p->slug]) }}">
                    <img src="{{ asset('assets/img/projetos/'.$p->capa) }}" alt="">
                    <div class="overlay">
                        <div>
                            {{ $p->titulo }}
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>
    
</div>

@endsection
