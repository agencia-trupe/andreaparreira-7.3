<div class="categorias2">
    @foreach($categorias as $c)
    <a href="{{ route('projetos', ['categoria_slug' => $c->slug]) }}" @if(isset($categoria) && $categoria->slug == $c->slug) class="active" @endif>
        {{ $c->titulo }}
    </a>
    @endforeach
</div>
