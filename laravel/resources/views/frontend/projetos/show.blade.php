@extends('frontend.common.template')

@section('content')

    <div class="projetos projetos-show">
        <div class="center-full" id="center-full">
            
            <div class="show desktop">
                <div class="pshow-center">
                    <div class="descricao">
                        <h2>{{ $projeto->titulo }}</h2>
                        <span>{{ $projeto->local }}</span>
                    </div>
                    <div class="pshow-videobox">
                        @if(count($projeto->videos))
                        @foreach($projeto->videos->chunk($projeto->videos->count())[0] as $v)
                        
                            <div class="video">
                                <iframe src="{{ $v->video }}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        
                        @endforeach
                        @endif
                    </div>
                    <div class="pshow-imagembox">
                        @if(count($projeto->imagens))
                        @foreach($projeto->imagens->chunk($projeto->imagens->count())[0] as $i)
                        <a href="{{ asset('assets/img/projetos/imagens/highres/'.$i->imagem) }}" class="projeto-imagem" rel="projeto" >
                            <img src="{{ asset('assets/img/projetos/imagens/'.$i->imagem) }}" alt="" class="img-projeto">
                        </a>
                        @endforeach
                        @endif

                    
                    </div>

                </div>
            </div>

            <div class="controls">
                <a href="{{ route('projetos', $projeto->categoria->slug) }}">
                    <div class="link-topo-box">    
                        <img src="{{ asset('assets/img/layout/ico-seta-voltar.png') }}" class="ico-seta-topo" alt="">
                        <p>voltar</p>
                    </div>
                </a>
             
                <a href="" class="link-topo" title="Voltar ao Topo">
                    <div class="link-topo-box">
                        <p class="control-voltar">topo</p>
                        <img src="{{ asset('assets/img/layout/ico-seta-topo.png') }}" class="ico-seta-topo" alt="">
                    </div>
                </a>

            </div>
        </div>
    </div>
  
    <div class="categorias3">
        @foreach($categorias as $c)
            <a href="{{ route('projetos', $c->slug) }}" @if(isset($categoria) && $categoria->slug == $c->slug) class="active" @endif>
                {{ $c->titulo }}
            </a>
        @endforeach
    </div>
@endsection
