    <footer>
        <div class="center">
            <div class="footer-contato">
                <div class="footer-redes">
                        @if($contato->telefone)
                            <p class="footer_telefone">{{ $contato->telefone }}</p></a>
                            <img src="{{ asset('assets/img/layout/ico-telefone.svg') }}" class="img_telefone" alt="">
                        @endif 
                </div>
                <div class="footer-redes2">
                        @if($contato->whatsapp)
                            <a href="https://api.whatsapp.com/send?phone=55{{ $contato->whatsapp }}" class="whatsapp" target="_blank"><p class="footer_whatsapp">+55 {{ $contato->whatsapp }}</p></a>
                            <img src="{{ asset('assets/img/layout/ico-whatsapp.svg') }}" class="img_whatsapp" alt="">
                        @endif 
                </div>
                <div class="footer-redes3">
                    @if($contato->instagram)
                    <a href="https://www.instagram.com/{{ $contato->instagram }}" class="footer-instagram" target="_blank">@ {{ $contato->instagram }}</a>
                    <img src="{{ asset('assets/img/layout/ico-instagram-branco.svg') }}" class="img_instagram" alt="">
                    @endif
                </div>
            </div>

            <div>
                <div class="footer-endereco">
                    <img src="{{ url('assets/img/layout/ico-endereco.svg') }}" class="footer-ico-endereco">
                    {!! $contato->endereco !!}
                </div>
                <a href="">
                    <div class="footer-email">{{ $contato->email }}
                    <img src="{{ asset('assets/img/layout/ico-email.svg') }}" class="img_email" alt="">
                    </div>
                </a>
            </div>
            <div class="footer-links">
                <div>
                    <a href="{{ route('politica-de-privacidade') }}" class="footer-politica">POLÍTICA DE PRIVACIDADE</a>
                    <a href="https://www.trupe.net">
                        &copy; {{ config('app.name') }} {{ date('Y') }} &middot; Todos os direitos reservados.<BR>
                        Criação de sites: Trupe Agência Criativa
                    </a>
                </div>
            </div>
        </div>
