    <header @if(Tools::routeIs('home')) class="header-home" @endif>
        <div class="center-header">
            <a href="{{ route('home') }}" class="logo">
                <img src="{{ asset('assets/img/layout/marca-andreaparreira.svg') }}" alt="">
            </a>

            @if(Tools::routeIs('projetos*'))
                <div class="submenu-home">
                    @include('frontend.projetos._categorias')
                </div>
            @endif

            <nav id="nav-desktop">
            
                <a href="{{ route('perfil') }}" @if(Tools::routeIs('perfil')) class="active" @endif>
                    perfil
                </a>
                <a href="{{ route('servicos') }}" @if(Tools::routeIs('servicos')) class="active" @endif>
                    serviços
                </a>
                <a href="{{ route('projetos') }}" @if(Tools::routeIs('projetos*')) class="highline" @endif>
                    portfólio
                </a>
                <a href="{{ route('clippings') }}" @if(Tools::routeIs('clippings')) class="active" @endif>
                    clipping
                </a>   
                <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>
                    contato
                </a>

                @if($contato->instagram)
                    <a href="https://www.instagram.com/{{ $contato->instagram }}" class="instagram" target="_blank">instagram</a>
                @endif
            </nav>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>
    </header>

    <nav id="nav-mobile">
        <div class="center-header">
        
            <a href="{{ route('perfil') }}" @if(Tools::routeIs('perfil')) class="active" @endif>
                perfil
            </a>
            <a href="{{ route('servicos') }}" @if(Tools::routeIs('servicos')) class="active" @endif>
                serviços
            </a>
            <a href="{{ route('projetos') }}" @if(Tools::routeIs('projetos*')) class="active" @endif>
                portfólio
            </a>
            <a href="{{ route('clippings') }}" @if(Tools::routeIs('clippings')) class="active" @endif>
                clipping
            </a>
            <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>
                contatos
            </a>

            @if($contato->instagram)
                <a href="https://www.instagram.com/{{ $contato->instagram }}" class="instagram" target="_blank">instagram</a>
            @endif
        </div>
    </nav>
