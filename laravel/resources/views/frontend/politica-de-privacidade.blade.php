@extends('frontend.common.template')

@section('content')

<section class="politica-de-privacidade">
    <h2 class="titulo">POLÍTICA DE PRIVACIDADE</h2>
    <div class="texto">
        {!! $politica->texto !!}
    </div>
</section>

@endsection